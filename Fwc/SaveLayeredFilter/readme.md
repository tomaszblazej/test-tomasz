# Test module Save Layered Size Filter

It's simple module that save filter 'size'.

Filter value is loaded from cookie on all categories that have products with loaded value.
For easy value change filter block for size is always visible.


Information
-
This version don't have implemented clearing saved attribute in user friendly way.

To clear loaded value please remove cookie "test-cookie-size" , and for varnish cache X-Magento-Vary


If magento throw Exception Bucket does not exist in category view - Set root category to anchored from Catalog -> Categories -> Display setting. If it is set already, set to 'No' then save, after that change to 'Yes' and save.


Tested on clean magento 2.3.1 with sample data.

Varnish
-
Compatibility with varnish is done by using page variations. There is ajax call to custom controller in category view that is sending url parameters. It is used for obtaining new value for X-Magento-Vary.  

