define([
    'jquery',
    'domReady!'
], function ($) {
    return function (config) {
        $.ajax({
            url: config.url + window.location.search,
            data: {
                isAjax: 1
            },
            type: "GET",
            dataType: 'json',
            success: function (data) {
            }
        });

    }
});
