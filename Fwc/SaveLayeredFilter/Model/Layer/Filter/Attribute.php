<?php
/**
 * @package   Fwc\SaveLayeredFilter
 * @author    Tomasz Błażej <tomasz.blazej@fastwhitecat.com>
 * @copyright 2019 Fast White Cat S. A.
 * @license   See LICENSE_FASTWHITECAT.txt for license details.
 */

namespace Fwc\SaveLayeredFilter\Model\Layer\Filter;

use Fwc\SaveLayeredFilter\Helper\Data;
use Fwc\SaveLayeredFilter\Model\Cookie;
use Magento\Catalog\Model\Layer;
use Magento\Catalog\Model\Layer\Filter\Item\DataBuilder;
use Magento\Catalog\Model\Layer\Filter\ItemFactory;
use Magento\CatalogSearch\Model\Layer\Filter\Attribute as FilterAttribute;
use Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\StateException;
use Magento\Framework\Filter\StripTags;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Attribute
 */
class Attribute extends FilterAttribute
{


    /**
     * @var Cookie
     */
    protected $cookie;

    /**
     * Attribute constructor.
     *
     * @param ItemFactory           $filterItemFactory
     * @param StoreManagerInterface $storeManager
     * @param Layer                 $layer
     * @param DataBuilder           $itemDataBuilder
     * @param StripTags             $tagFilter
     * @param Cookie                $cookie
     * @param array                 $data
     */
    public function __construct(
        ItemFactory $filterItemFactory,
        StoreManagerInterface $storeManager,
        Layer $layer,
        DataBuilder $itemDataBuilder,
        StripTags $tagFilter,
        Cookie $cookie,
        array $data = []
    ) {
        parent::__construct(
            $filterItemFactory,
            $storeManager,
            $layer,
            $itemDataBuilder,
            $tagFilter,
            $data
        );

        $this->cookie = $cookie;
    }

    /**
     * @param RequestInterface $request
     *
     * @return $this|FilterAttribute
     * @throws InputException
     * @throws LocalizedException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    public function Apply(RequestInterface $request)
    {
        $attributeValue = $this->getParamsAttribute($request);

        if (empty($attributeValue) && !is_numeric($attributeValue)) {
            return $this;
        }

        $attribute = $this->getAttributeModel();
        /** @var Collection $productCollection */
        $productCollection = $this->getLayer()
            ->getProductCollection();

        $productCollection->addFieldToFilter($attribute->getAttributeCode(), $attributeValue);
        $label = $this->getOptionText($attributeValue);
        $this->getLayer()
            ->getState()
            ->addFilter($this->_createItem($label, $attributeValue));

        if ($attribute->getAttributeCode() == Data::SIZE_ATTRIBUTE) {
            return $this;
        }

        $this->setItems([]); // set items to disable show filtering

        return $this;
    }

    /**
     * @param RequestInterface $request
     *
     * @return mixed|string|null
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     * @throws InputException
     * @throws LocalizedException
     * @throws StateException
     */
    protected function getParamsAttribute(RequestInterface $request)
    {

        $attributeValue = $request->getParam($this->_requestVar);

        if ($this->getRequestVar() != self::SIZE_ATTRIBUTE) {
            return $attributeValue;
        }
        if ($request->getParam(self::SIZE_ATTRIBUTE)) {

            $this->cookie->set($request->getParam(self::SIZE_ATTRIBUTE));

            return $attributeValue;
        }

        //find if collection have products with desired attribute
        $attribute = $this->getAttributeModel();
        /** @var Collection $productCollection */
        $productCollection = $this->getLayer()
            ->getProductCollection();
        $cloned            = clone $productCollection; // fix for illegal state
        //  After searching collection it will throw exception if we add new attribute
        $optionsFacetedData = $cloned->getFacetedData($attribute->getAttributeCode());

        $cookieValue = $this->cookie->get();
        if (!isset($optionsFacetedData[$cookieValue])) {
            return $attributeValue;
        }
        $attributeValue               = $cookieValue;
        $params                       = $request->getParams();
        $params[self::SIZE_ATTRIBUTE] = $attributeValue;
        $request->setParams($params);


        return $attributeValue;
    }
}