<?php
/**
 * @package   Fwc\SaveLayeredFilter
 * @author    Tomasz Błażej <tomasz.blazej@fastwhitecat.com>
 * @copyright 2019 Fast White Cat S. A.
 * @license   See LICENSE_FASTWHITECAT.txt for license details.
 */

namespace Fwc\SaveLayeredFilter\Model;

use Fwc\SaveLayeredFilter\Helper\Data;
use Magento\Catalog\Model\Product\ProductList\Toolbar as ToolbarModel;
use Magento\Framework\App\Http\Context;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\InputException;
use Magento\Framework\Stdlib\Cookie\CookieSizeLimitReachedException;
use Magento\Framework\Stdlib\Cookie\FailureToSendException;

/**
 * Class SizeAttributeContextPlugin
 */
class SizeAttributeContextPlugin
{
    /**
     * @var Cookie
     */
    protected $cookie;

    /**
     * @var Http
     */
    protected $request;

    /**
     * SizeAttributeContextPlugin constructor.
     *
     * @param Cookie $cookie
     * @param Http   $request
     */
    public function __construct(
        Cookie $cookie,
        Http $request
    ) {
        $this->cookie  = $cookie;
        $this->request = $request;
    }

    /**
     * @param Context $subject
     *
     * @return array
     * @throws InputException
     * @throws CookieSizeLimitReachedException
     * @throws FailureToSendException
     */
    function beforeGetVaryString(Context $subject)
    {
        if ($sizeValue = $this->request->getParam(Data::SIZE_ATTRIBUTE)) {
            $this->cookie->set($sizeValue);
        } else {
            $sizeValue = $this->cookie->get();
        }

        $subject->setValue('size_attribute', $sizeValue, '');

        return [];
    }

}