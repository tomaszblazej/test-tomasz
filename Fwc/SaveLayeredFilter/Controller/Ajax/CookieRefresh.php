<?php
/**
 * @package   Fwc\SaveLayeredFilter
 * @author    Tomasz Błażej <tomasz.blazej@fastwhitecat.com>
 * @copyright 2019 Fast White Cat S. A.
 * @license   See LICENSE_FASTWHITECAT.txt for license details.
 */

namespace Fwc\SaveLayeredFilter\Controller\Ajax;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Page;

/**
 * Class CookieRefresh
 */
class CookieRefresh extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * CookieRefresh constructor.
     *
     * @param Context     $context
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * @return ResponseInterface|ResultInterface|Page
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $result->setHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0', true);

        $result->setData([]);

        return $result;
    }
}