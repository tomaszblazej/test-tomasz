<?php
/**
 * @package   Fwc\SaveLayeredFilter
 * @author    Tomasz Błażej <tomasz.blazej@fastwhitecat.com>
 * @copyright 2019 Fast White Cat S. A.
 * @license   See LICENSE_FASTWHITECAT.txt for license details.
 */

namespace Fwc\SaveLayeredFilter\Block;

use Magento\Framework\View\Element\Template;

/**
 * Class RefreshCookie
 */
class cookieRefresh extends Template
{

    /**
     * @return string
     */
    public function getAjaxUrl()
    {
        return $this->getUrl('saveLayeredFilter/ajax/cookieRefresh');
    }
}